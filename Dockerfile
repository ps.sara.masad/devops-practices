FROM openjdk:8-jre-alpine
ARG ARG_JAVA_OPTS
ARG ARG_PROFILE
ENV JAVA_OPTS=$ARG_JAVA_OPTS
ENV PROFILE=$ARG_PROFILE
COPY target/*.jar app.jar
CMD java $JAVA_OPTS -Dspring.profiles.active=$PROFILE -jar app.jar